# German translations for Glaxnimate Manual package
# German translation for Glaxnimate Manual.
# Copyright (C) licensed under the  <a href="https://creativecommons.org/licenses/by-sa/4.0/">Creative Commons License SA 4.0</a> unless stated otherwise
# This file is distributed under the same license as the Glaxnimate Manual package.
# Automatically generated, 2024.
#
msgid ""
msgstr ""
"Project-Id-Version: docs_glaxnimate_org_tutorials\n"
"Report-Msgid-Bugs-To: \n"
"POT-Creation-Date: 2024-04-30 01:45+0000\n"
"PO-Revision-Date: 2024-04-30 08:46+0200\n"
"Last-Translator: Automatically generated\n"
"Language-Team: none\n"
"Language: de\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=2; plural=(n != 1);\n"

#: ../../tutorials.rst:1
msgid "Glaxnimate Tutorials"
msgstr ""

#: ../../tutorials.rst:1
msgid ""
"KDE, Glaxnimate, tips, tricks, tips & tricks, vector, animation, "
"documentation, user manual, video editor, open source, free, learn, easy"
msgstr ""

#: ../../tutorials.rst:15
msgid "Tutorials"
msgstr ""

#: ../../tutorials.rst:18
msgid "Written Tutorials"
msgstr ""

#: ../../tutorials.rst:28
msgid "Video Tutorials"
msgstr ""

#: ../../tutorials.rst:30
msgid "This is a collection of various unoffical video tutorials:"
msgstr ""

#: ../../tutorials.rst:32
msgid ""
"`Shotcut Motion Masking Effect Using Glaxnimate <https://www.youtube.com/"
"watch?v=p72VQ7kNDeg>`_ (English)"
msgstr ""

#: ../../tutorials.rst:33
msgid ""
"`Easy Character Animation in Glaxnimate <https://www.youtube.com/watch?"
"v=REU8Z5j7qlw>`_ (English)"
msgstr ""

#: ../../tutorials.rst:34
msgid ""
"`Glaxnimate - Eses infor Learning <https://www.youtube.com/playlist?"
"list=PLqlTgdmIZxTvJ98FtjTUbMVkGgA4bRAa0>`_ (English)"
msgstr ""

#: ../../tutorials.rst:35
msgid ""
"`Shotcut Tutorial - Draw an animated itinerary with Glaxnimate <https://www."
"youtube.com/watch?v=8mpJFPvJl6c>`_ (English)"
msgstr ""

#: ../../tutorials.rst:36
msgid ""
"`Glaxnimate - punto edu <https://www.youtube.com/playlist?"
"list=PLNdcfEqn_DGDxp6Vx1RRFZpfnHreXBn8R>`_ (Spanish)"
msgstr ""

#: ../../tutorials.rst:37
msgid ""
"`Offset de Trim path y otros efectos en Glaxnimate - Rafa <https://www."
"youtube.com/watch?v=bcWKhdFHEDY>`_ (Spanish)"
msgstr ""
