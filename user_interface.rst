.. meta::
   :description: Introduction to Glaxnimate's User Interface
   :keywords: KDE, Glaxnimate, user interface, documentation, user manual, vector animation, 2D, open source, free, learn, easy, user interface, layout, widget

.. metadata-placeholder

   :authors: - Mattia Basaglia
             
   :license: Creative Commons License SA 4.0

.. _user_interface:

##############
User Interface
##############


.. figure:: /images/main_window/main_window.png
  :align: center
  :width: 650px
  :alt: Main Window

  Main Windows

Glaxnimate user interface has the following components:


The :ref:`canvas` is the center of the Glaxnimate interface and is where you will preview and edit your animations.

Located around the canvas are the :ref:`dockable_views`. These provide quick access to all of the main functionality of Glaxnimate and can be hidden or re-arranged to fit your taste. Some of the dockable views include the Properties, Layers, and the Timeline.

At the top of the Glaxnimate interface, you'll find the :ref:`menus` and :ref:`toolbars`.
These work in the same way as most user interfaces and provide access to a variety of tools and options that you'll use to create your animations.


.. toctree::
   :hidden:
   :glob:

   user_interface/*
