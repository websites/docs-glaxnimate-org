# Official Documentation for Glaxnimate

[Link to Glaxnimate Sphinx documentation](https://docs.glaxnimate.org)

Glaxnimate documentation based on [Sphinx](https://www.sphinx-doc.org)

## Setting up Development Environment

1. First you need to install [python3](https://www.python.org) and PIP as it is required to install sphinx.
2. You can check whether python was installed successfully (and your version is 3 and not 2) by running `python --version`
3. Now you can sphinx and the sphinx theme we are using with `python3 -m pip install --upgrade sphinx sphinx_rtd_theme`
4. You can check whether sphinx was installed successfully by running `sphinx-build --version`

## Running sphinx

After you cloned this repository (only need to be done once), use a command line to go to its root folder (e.g. with `cd /path/to/glaxnimate-docs`)

### To generate a html web documentation run  

**Linux:** `make html`  
**Windows:** `.\make html`

The HTML is generated in `build/html` (e.g. with `/path/to/glaxnimate-docs/build/html`). Open the web document by double click `index.html`.

### To generate an epub ebook run  

**Linux:** `make epub`  
**Windows:** `.\make epub`

The epub ebook is generated in `build/epub` (e.g. with `/path/to/glaxnimate-docs/build/epub`). Open the ebook by double click `index.xhtml`.

## Enable languages

1. Enable the build of the language in `.gitlab-ci.yml` via the `WEBSITE_LANGUAGES` variable
2. Tell Sphinx the language is supported by adding it to `$supported_languages` languages in `404handler.php`
3. Add the language to the language chooser by adding it to `all_langs` in `/resources/static/js/version_switch.js`

## Get involved

Contribute to this Manual. [More details see here](https://glaxnimate.mattbas.org/contributing/documentation/).
  
## Status of translation
  
The status of the Glaxnimate documentation you can see [here](https://l10n.kde.org/stats/gui/trunk-kf5/package/documentation-docs-glaxnimate-org/).
