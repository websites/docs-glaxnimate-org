.. metadata-placeholder

   :authors: - Mattia Basaglia

   :license: Creative Commons License SA 4.0


.. _menus:

Menus
=====

File
----

.. figure:: /images/menus/file.png
   :width: 250px
   :alt: file menu

   The usual File menu actions.

Edit
----

.. figure:: /images/menus/edit.png
   :width: 250px
   :alt: edit menu

Tools
-----

.. figure:: /images/menus/tools.png
   :width: 250px
   :alt: tools menu

   This menu toggles which is the active :ref:`tools`.

View
----

.. figure:: /images/menus/view.png
   :width: 250px
   :alt: view menu

Views
~~~~~

.. figure:: /images/menus/views.png
   :width: 250px
   :alt: views menu

   This menu toggles which :ref:`dockable_views` are visible.


Toolbar Menu
~~~~~~~~~~~~

.. figure:: /images/menus/toolbars.png
   :width: 250px
   :alt: toolbars menu

   This menu toggles which :ref:`toolbars` are visible.


Document
--------

.. figure:: /images/menus/document.png
   :width: 250px
   :alt: document menu

Single frame
~~~~~~~~~~~~

.. figure:: /images/menus/render_single_frame.png
   :width: 250px
   :alt: render single frame menu

Web preview
~~~~~~~~~~~

.. figure:: /images/menus/web_preview.png
   :width: 250px
   :alt: web preview menu

The actions in this menu will open your browser to an html page that embeds the animation using the exporting options associated with said action.

Playback
--------

.. figure:: /images/menus/playback.png
   :width: 250px
   :alt: playback menu

Layers
------

.. figure:: /images/menus/layers.png
   :width: 250px
   :alt: layers menu

New layer
~~~~~~~~~

.. figure:: /images/menus/new_layer.png
   :width: 250px
   :alt: new layer menu

Object
------

.. figure:: /images/menus/object.png
   :width: 250px
   :alt: object menu

Align
~~~~~

.. figure:: /images/menus/align.png
   :width: 250px
   :alt: align menu

Path
----

.. figure:: /images/menus/path.png
   :width: 250px
   :alt: path menu

Help
----

.. figure:: /images/menus/help.png
   :width: 250px
   :alt: help menu

.. _toolbars:

Toolbars
========

Main
----

.. figure:: /images/toolbars/main.png
   :width: 650px
   :alt: main toolbar

Node Editing
------------

.. figure:: /images/toolbars/node.png
   :width: 650px
   :alt: node toolbar

   This toolbar is only present when you have the edit tool active.

Tools
-----

.. figure:: /images/toolbars/tools.png
   :width: 650px
   :alt: tools toolbar

.. _status_bar:

Status Bar
==========

.. figure:: /images/main_window/status_bar.png
   :width: 650px
   :alt: Status bar

   The status bar shows you whether you are in recording mode, the mouse position, the current fill/stroke colors, and various widgets to adjust the canvas transform.
