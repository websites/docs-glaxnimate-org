.. metadata-placeholder

   :authors: - Mattia Basaglia

   :license: Creative Commons License SA 4.0

.. _dialogs:

Dialogs
=======

About
-----

Shows various information about Glaxnimate.

Credits
~~~~~~~

.. figure:: /images/dialogs/about_credits.png
  :align: left
  :width: 250px
  :alt: dialogs/About dialog

  About dialog

General information and links.

Paths
~~~~~

.. figure:: /images/dialogs/about_paths.png
   :align: left
   :width: 250px
   :alt: About dialog

   About dialog

Here you can see and open the settings file, and all paths where Glaxnimate looks for data.

System
~~~~~~

.. figure:: /images/dialogs/about_system.png
   :align: left
   :width: 250px
   :alt: About dialog

   About dialog

Shows the system information. You can click on :guilabel:`Copy` to copy them to the clipboard.

Move To
-------

.. figure:: /images/dialogs/move_to.png
   :align: left
   :width: 250px
   :alt: Move To dialog

   Move To dialog

Allows you to choose a destination to move the selected shapes.

Resize
------

.. figure:: /images/dialogs/resize.png
   :align: left
   :width: 250px
   :alt: Resize dialog

   Resize dialog

Allows to resize the animation and scale all layers.

Timing
------

.. figure:: /images/dialogs/timing.png
   :align: left
   :width: 250px
   :alt: Timing dialog

   Timing dialog

Allows you to change framerate and duration.

.. _trace_bitmap:

Trace Bitmap
------------

This dialog allows you to convert raster images (bitmaps) into vector paths.

Closest Color
~~~~~~~~~~~~~

.. figure:: /images/dialogs/trace/closest.png
   :align: left
   :width: 250px
   :alt: Trace dialog

   Trace dialog

In the :guilabel:`Closest Color` mode, you can select a set of colors and they will be used to determine how shapes are defined.

This algorithm works by using the color on the list that most closely matches the pixel values, and pixels with the same resulting color will end up in the same shape.

You can edit the list of colors by using the buttons to add or remove colors, then you can click on the list to change the color.

Alternatively, you can click on :guilabel:`Detect Colors` which will fill the list with the most common colors in the image (you select how many colors you want with the spin box next to the button.

Sometimes there might be gaps between shapes of different colors, you can increase :guilabel:`Outline` to fill them up. When you have large outlines, the order of the colors in the list is important (shapes higher up on the list are drawn over the ones below). You can drag and drop colors in the list to rearrange them.

Extract Colors
~~~~~~~~~~~~~~

.. figure:: /images/dialogs/trace/extract.png
   :align: left
   :width: 250px
   :alt: Trace dialog

   Trace dialog

This mode has the same image options as :guilabel:`Closest Color` but it will only use pixels that match the specified colors exactly.

The difference is more obvious when you have fewer colors; in the following example the two modes are operating on the same image with the same colors:

.. rst-class:: clear-both

.. figure:: /images/dialogs/trace/extract_fewer.png
   :align: left
   :width: 250px
   :alt: Trace dialog

   Trace dialog

.. figure:: /images/dialogs/trace/closest_fewer.png
   :align: left
   :width: 250px
   :alt: Trace dialog

   Trace dialog

.. rst-class:: clear-both

As you can see, :guilabel:`Extract Colors` only used the parts of the image that had matching colors, resulting in fewer shapes being extracted; while :guilabel:`Closest Color` assigned the best available color for each area.

Generally, :guilabel:`Closest Color` is the best choice when you want to trace the whole image, :guilabel:`Extract Colors` when you only need specific elements.

:guilabel:`Extract Colors` has the option :guilabel:`Tolerance`, which determines how close the pixels have to be to the specified color.

Transparency
~~~~~~~~~~~~

.. figure:: /images/dialogs/trace/alpha.png
   :align: left
   :width: 250px
   :alt: Trace dialog

   Trace dialog

This mode ignore colors and just looks at the transparency (Alpha channel) of the image.

It has the following options:

* Output Color - This is the color of the shapes you get
* Invert - Will generate the inverse image: shapes will occupy the transparent areas
* Threshold - Alpha value below which a pixel is considered transparent

This mode is best suited for converting monochrome logos and the like.


Pixel
~~~~~

.. figure:: /images/dialogs/trace/pixel.png
   :align: left
   :width: 250px
   :alt: Trace dialog

   Trace dialog

This mode is designed for pixel art and is only available for small pictures.

It keeps the edges of every pixel intact and uses all colors from the source image.

With this mode there are no extra options.

Curve Options
~~~~~~~~~~~~~

These options affect the appearance of the output shapes

* Smoothness - A value of 0% will create polygons, a value of 100% will avoid any sharp corners
* Minimum Area - If a region with the same color doesn't have more than these many pixels, it will be discarder. Useful to remove speckles.


Preview
~~~~~~~

The right half of the dialog shows a preview of the operation.

Since tracing might be slow, you need to press :guilabel:`Update` for the preview to be updated.

The slider below the preview area shows the tracing result side by side with the original image. The area to the left of the slider marker shows the trace result, while the area to the right shows the original image. By default the slider is all the way to the right, only revealing the trace result.


Emoji Selection
---------------

Glaxnimate support downloading and importing emoji as a form of vector assets.

All the supported sets have a free and open source license so you can use them in you animations as long as you respect their licensing terms.

Emoji Sets
~~~~~~~~~~

.. figure:: /images/dialogs/emoji/emoji_set_dialog.png
   :align: left
   :width: 250px
   :alt: Emoji Set Dialog

   Emoji Set Dialog

Here you can manage the installed sets.

By default no emoji set is installed. To install them, you can select one and click download. The ones you already downloaded show a checkmark at the end.

On the list of available sets you see their name, license, a preview of some emoji, and the download status.

Note that not all sets support all emoji, and this is reflected on the previews.

Clicking :guilabel:`Reload` will update the list of available emoji based on the [emoji data file](/contributing/assets/#emoji-sets).

When you have a set selected, you can click on one of the following buttons:

* :guilabel:`View Website` Will open the browser to the website of the selected set
* :guilabel:`Download` Will download the set
* :guilabel:`Add Emoji..` Will show the emoji selection dialog to import emoji in your animation


Emoji Select Dialog
~~~~~~~~~~~~~~~~~~~

.. figure:: /images/dialogs/emoji/emoji_select_dialog.png
   :align: left
   :width: 250px
   :alt: Emoji Select Dialog

   Emoji Select Dialog

This dialog shows you all the emoji for the set you have selected.

At the top you have buttons to jump to a specific emoji category.

Clicking on an emoji will import it in you animation as a precomposition:

.. figure:: /images/dialogs/emoji/result.png
   :align: left
   :width: 250px
   :alt: Imported emoji

   Imported emoji
