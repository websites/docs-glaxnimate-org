.. metadata-placeholder

   :authors: - Mattia Basaglia

   :license: Creative Commons License SA 4.0


.. _loading_spinner:

Loading Spinner
===============

This tutorial will show how to create a simple loading spinner animation.

This highlights how to use the Trim Path feature.

.. lottie::
  :json: /files/spinner.json
  :rawr: /files/spinner.rawr
  :height: 256px
  :width: 256px

Draw a Circle
-------------

Draw a circle for the spinner. You can do this by selecting the "Ellipse" tool from the toolbar. Ensure in the tool options the Fill is disabled and Stroke is enabled. Click and drag on the canvas to create a circle of the desired size. If you hold Ctrl, Glaxnimate will ensure the ellipse is a perfect circle.

.. figure:: /images/tutorials/draw-ellipse.png
  :align: left
  :width: 250px
  :alt: Drawing the circle

  Drawing the circle

You can change the stroke color and thickness, as well the cap style to Round.

.. rst-class:: clear-both

.. figure:: /images/tutorials/stroke-style.png
  :align: left
  :width: 250px
  :alt: Stroke Style

  Stroke Style

To align the circle at the center of the canvas you can use the align view, just make sure you select :guilabel:`Canvas` from the dropdown.

.. figure:: /images/tutorials/align.png
  :align: left
  :width: 250px
  :alt: Aligning to the center

  Aligning to the center

Trim Path
---------

The Trim Path is a modifier that allows you to animate a shape being "drawn" and similar effects.

We will use it to make the loading spinner appear, go around, and disappear.

To add it, you can right click on the :guilabel:`Ellipse` group in the timeline, then select :guilabel:`Add` and :guilabel:`Trim Path`.

.. figure:: /images/tutorials/add-trim.png
  :align: left
  :width: 250px
  :alt: Adding Trim Path

  Adding Trim Path

The animation is going to be 60 frames long, ensure you have the correct timing from the document timing dialog.

.. rst-class:: clear-both

.. figure:: /images/tutorials/timing-dialog.png
  :align: left
  :width: 250px
  :alt: Adjusting the timing

  Adjusting the timing


Adding Keyframes
----------------

We will now add some keyframes on the trim path properties.

To add a keyframe ensure you have the correct frame selected, change the property value, then right click on the property track on the timeline and select :guilabel:`Add Keyframe`.

At frame 0 add a keyframe setting :guilabel:`end` and :guilabel:`offset` to 0%. This will hide the circle as there's nothing between :guilabel:`start` and :guilabel:`end` on the trim path.

.. figure:: /images/tutorials/keyframe-0.png
  :align: left
  :width: 250px
  :alt: Keyframe 0

  Keyframe 0

.. rst-class:: clear-both

At frame 20 add a keyframe setting :guilabel:`end` to 33%. This adds the first phase of the animation, a segment of the circle appearing from the top.


.. figure:: /images/tutorials/keyframe-20.png
  :align: left
  :width: 250px
  :alt: Keyframe 20

  Keyframe 20

.. rst-class:: clear-both

At frame 40 add a keyframe setting :guilabel:`start` to 0%.

.. figure:: /images/tutorials/keyframe-40.png
  :align: left
  :width: 250px
  :alt: Keyframe 40

  Keyframe 40

.. rst-class:: clear-both

At frame 60 add a keyframe setting :guilabel:`start` to 33% and :guilabel:`offset` to 67%. This will make the spinner disappear in a similar fashion to how it appeared.

.. figure:: /images/tutorials/keyframe-60.png
  :align: left
  :width: 250px
  :alt: Keyframe 60

  Keyframe 60
