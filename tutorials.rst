.. meta::
   :description: Glaxnimate Tutorials
   :keywords: KDE, Glaxnimate, tips, tricks, tips & tricks, vector, animation, documentation, user manual, video editor, open source, free, learn, easy

.. metadata-placeholder

   :authors: - Mattia Basaglia

   :license: Creative Commons License SA 4.0


.. _tutorials:

Tutorials
=========

Written Tutorials
-----------------

.. toctree::
   :maxdepth: 1
   :glob:

   tutorials/*


Video Tutorials
---------------

This is a collection of various unoffical video tutorials:

* `Shotcut Motion Masking Effect Using Glaxnimate <https://www.youtube.com/watch?v=p72VQ7kNDeg>`_ (English)
* `Easy Character Animation in Glaxnimate <https://www.youtube.com/watch?v=REU8Z5j7qlw>`_ (English)
* `Glaxnimate - Eses infor Learning <https://www.youtube.com/playlist?list=PLqlTgdmIZxTvJ98FtjTUbMVkGgA4bRAa0>`_ (English)
* `Shotcut Tutorial - Draw an animated itinerary with Glaxnimate <https://www.youtube.com/watch?v=8mpJFPvJl6c>`_ (English)
* `Glaxnimate - punto edu <https://www.youtube.com/playlist?list=PLNdcfEqn_DGDxp6Vx1RRFZpfnHreXBn8R>`_ (Spanish)
* `Offset de Trim path y otros efectos en Glaxnimate - Rafa <https://www.youtube.com/watch?v=bcWKhdFHEDY>`_ (Spanish)

.. toctree::
   :hidden:
   :glob:

   tutorials/*
